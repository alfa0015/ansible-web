variable "do_token" {}
variable "ssh-key-name" {
  type = string
  description = "ssh key name on digital ocean to attach a vm"
}
variable "vms-list" {
  type = list(object({
    image = string
    name = string
    region  = string
    size = string
  }))
  description = "numbers of webservers"
  default = [
    {
      image = "centos-7-x64"
      name  = "loadbalancer"
      region = "nyc1"
      size  = "s-1vcpu-1gb"
    }
  ]
}