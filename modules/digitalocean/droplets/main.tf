data "digitalocean_ssh_key" "default" {
  name = var.ssh-key-name
}

resource "digitalocean_droplet" "vm" {
  for_each   = {
		for index, vm in var.vms-list: index => vm
	}
  image = each.value.image
  name = each.value.name
  region = each.value.region
  size = each.value.size
  ssh_keys = [data.digitalocean_ssh_key.default.id]
}
