provider "digitalocean" {
  token = var.do_token
}
module "webserver" {
  source = "./modules/digitalocean/droplets"
  do_token = var.do_token
  vms-list = var.webservers_list
  ssh-key-name = var.ssh-key-name
}
module "loadbalancer" {
  source = "./modules/digitalocean/droplets"
  do_token = var.do_token
  vms-list = var.loadbalancer_list
  ssh-key-name = var.ssh-key-name
}
