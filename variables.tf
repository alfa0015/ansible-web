variable "do_token" {}
variable "ssh-key-name" {
  type = string
  description = "ssh key name on digital ocean to attach a vm"
}
variable "webservers_list" {
  type = list(object({
    image = string
    name = string
    region  = string
    size = string
  }))
  description = "numbers of webservers"
  default = [
    {
      image = "centos-7-x64"
      name  = "webserver-01"
      region = "nyc1"
      size  = "s-1vcpu-1gb"
    },
    {
      image = "centos-7-x64"
      name  = "webserver-02"
      region = "nyc1"
      size  = "s-1vcpu-1gb"
    },
  ]
}
variable "loadbalancer_list" {
  type = list(object({
    image = string
    name = string
    region  = string
    size = string
  }))
  description = "numbers of webservers"
  default = [
    {
      image = "centos-7-x64"
      name  = "haproxy-01"
      region = "nyc1"
      size  = "s-1vcpu-1gb"
    },
  ]
}
